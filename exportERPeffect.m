function outf = exportERPeffect(savefile, inp, design, comp, baseline, loc, latency, visual, singletrial)
%
%     savefile              excel file location and name
%
%     inp                   input structure with the following fields
%
%         inp.data          a cell of CHANNEL x TIME x SUBJECT ERP matrixes. 
%                           The cell design should correspond to a factorial design (e.g. 2x3)
%         inp.labels        substructure holding factor labels in field names
%                           and level labels in a cell within each field
%         inp.chanlocs      EEGLAB channel location structure corresponding
%                           to number of CHANNELS dimension
%         inp.times         EEGLAB times vector corresponding to the TIME
%                           dimension
%         inp.subs          cell of strings corresponding to the SUBJECT
%                           dimension
%
%     design                a cell specifying the factor levels to be included, 
%                           e.g. {1:2,1:2,1,2,1:2} or {':', ':', 1, 2, ':'}
%
%     comp                  a cell array of componet descriptions, different components in different rows: 
%
%         1st column        name (string) 
%         2nd column        cell array of channel labels
%         3rd column        time window (vector with beginning and end times in inp.times units)
%
%     baseline              optional new baseline substraction
%
%     loc                   optional what to do with electrodes
%
%         'ch'              export only separate channels
%         'roi'             export only pooled region of interest
%         'both'            export both
%
%     latency               optional latency export
%
%         'pl'              export only peak latency
%         'al'              export only 50% area latency
%         'both'            export both 
%         NaN               do not export latency
%
%     visual                an optional flag indicating wheather to show visual feedback on peak detection
%
%     singletrial           'yes' or 'no' if single trials export is also included
%% PREPARATIONS %%

    data = inp.data(design{:});                                            % select the data to be used based on specified design

    if nargin < 6
        loc = 'both';
    end
    
    if nargin < 7
        latency = NaN;
    end

    if nargin < 8
        visual = NaN;
    end
    
    if nargin < 9
        singletrial = 'no'; 
    end
    
%% BASELINE CORRECT %%
    
    if nargin < 5                                                          % if not set don't correct
    else
        [a, lbl] = min(abs(inp.times-baseline(1)));                        % find the indices of selected time windows
        [a, ubl] = min(abs(inp.times-baseline(2)));
        for d = 1:numel(data)
            data{d} = data{d}-repmat(mean(data{d}(:,lbl:ubl,:),2),[1, size(data{d},2),1]);
        end
    end
    
%%  SELECT DATA AND LABELS %%
    
    lbls = {}; flds = fields(inp.labels);                                  % makes a cell of only the selected labels 
    for d = 1:length(design)
        S.type = '()'; S.subs = design(d);
        lbls{d} = subsref(inp.labels.(flds{d}),S);
    end
    
    vars = {}; inds = {};                                                   % make a list of concatenated labels and indices using the "odometer" method
    upto = size(data); at = ones(1,length(size(data)));                     % min and upto levels (the former will cycle)
    if length(upto) == 2 && any(upto==1); 
        upto = upto(upto~=1); at = at(upto~=1);                             % remove the second singleton dimenison for 1-WAY designs
    end
    while length(vars) < numel(data);                                       % repeat until all possible levels have been found
        row.L = ''; row.D = '';                                             % initiate row counters
        for i = 1:length(at)                                                % cycle through all avaailable variables
            row.L = [row.L '_' lbls{i}{at(i)}];                             % generate a label for the taken variable and level
            row.D = [row.D ',' num2str(at(i))];                             % generate indices for the given variable and level
        end
        vars{end+1} = row.L(2:end);                                         % add the generated rows to the growing lists
        inds{end+1} = row.D(2:end);
        at(end) = at(end) + 1;                                              % shift the last ring on the odometer
        for i = length(at):-1:1                                             % cycle through all variables
            if at(i) > upto(i);                                             % if any of them has reached their limit, reset this one and shift the previous one to next level
                at(i) = 1;
                if i > 1
                    at(i-1) = at(i-1)+1;            
                end
            end
        end
    end
    

%% CONSTRUCT EXPORT FILES %% 

    allout = {};
    for c = 1:size(comp,1)                                                      % cycle through components to export

        [a, ltind] = min(abs(inp.times-comp{c,3}(1)));                          % find the indices of selected time windows
        [a, utind] = min(abs(inp.times-comp{c,3}(2)));
        inptimes = inp.times(ltind:utind);                                      % make a shortened time vector

        for ch = 1:length(comp{c,2})                                            % find channel indices
            chind(ch) = strmatch(comp{c,2}{ch},{inp.chanlocs.labels}, 'exact');
        end
        
        for d = 1:numel(data)                                                   % loop through experimental conditions    
            
            for locType = {'roi', 'ch'}                                         % loop through making ROI and single channel output

                if ~strcmp(loc,locType{:}) && ~strcmp(loc,'both')               % if the selected locType will not be exported
                    continue
                end
                
                if d == 1
                    outf.([locType{:} '_amp']) = {};
                    if any(strcmp(latency,{'pl', 'both'}))
                        outf.([locType{:} '_pl']) = {};
                        outf.([locType{:} '_peakAmp']) = {};
                        
                    end
                    if any(strcmp(latency,{'al', 'both'}))
                        outf.([locType{:} '_al']) = {};  % initiate variables   
                        outf.([locType{:} '_peakAmp']) = {};

                    end
                    
                    if strcmp(singletrial,'yes')
                        outf.([locType{:} '_singletrial_amp']) = [{'subject' 'component'} flds' 'channel' inp.epochFields {'data'}];
                        outf.([locType{:} '_singletrial_amp_splithalf']) = {'subject' 'cond' 'odd' 'even'};
                        outf.([locType{:} '_singletrial_amp_SNR']) = {'subject' 'cond' 'Mean' 'SD'};
                    end

                end
                
                for ch = chind;                                                 % loop through channels

                    if strcmp(locType,'roi')
                        fieldName = [comp{c,1} '_' vars{d}];
                        chSel = 'chind';
                        if ch~=chind(1)                                         % when making ROI output loop only a single round
                            continue
                        end
                    elseif strcmp(locType,'ch')
                        fieldName = [comp{c,1} '_' vars{d} '_' inp.chanlocs(ch).labels];
                        chSel = 'ch';
                    end                                    
                    dataCall = ['data{' inds{d} '}(' chSel ',ltind:utind,:)'];
                        
                    % AMPLITUDE %
                    outf.([locType{:} '_amp'])(:,end+1) = cat(1,fieldName,...
                        mat2cell(squeeze(mean(mean(eval(dataCall),2),1)),ones(size(data{1},3),1),1));
                    
                    if strcmp(singletrial,'yes')
                        for s = 1:length(inp.subs)
                            call = ['inp.raw.dat{' strcat(inds{d},',',num2str(s)) '}(chind,ltind:utind,:)'];
                            datcol = squeeze(mean(mean(eval(call),2),1));
                            bkgcol = eval(['inp.raw.inf{' strcat(inds{d},',',num2str(s)) '}']);
                            infocol = [inp.subs(s) strsplit(fieldName,'_')];
                            outf.([locType{:} '_singletrial_amp'])(end+1:end+length(datcol),:) = cat(2,repmat(infocol,[length(datcol),1]),bkgcol,mat2cell(datcol,ones(size(datcol)),1));
                    
                            % RELIABILTY
                            outf.([locType{:} '_singletrial_amp_splithalf'])(end+1,:) = {infocol{1} fieldName mean(datcol(1:2:end)) mean(datcol(2:2:end))};
                            outf.([locType{:} '_singletrial_amp_SNR'])(end+1,:) = {infocol{1} fieldName mean(datcol) std(datcol)};
                        end                                                        
                    end
              
                    % LATENCY %
                    if any(strcmp(latency,{'pl', 'both'}))

                        seg = squeeze(mean(eval(dataCall),1));                  % isolate the component for each subject 
                        if any(comp{1}=='N'); seg = -1*seg; end                 % for negative components flip the waveform around

                        % peak latency %
                        peak = struct();
                        for e = 1:size(seg,2)
                            a = seg(:,e);
                            [peak(e).lat, peak(e).amp] = peakfinder(a);
                            if any(comp{1}=='N'); peak(e).amp = -1*peak(e).amp; end
                            peak(e).lat = inptimes([peak(e).lat]);              % convert to actual durations
                            if length(peak(e).lat)>1                            % kui leiab rohkem kui �he tipu
                                % kui on ��red j�ta need v�lja
                                [hval, hloc] = max(peak(e).amp);
                                peak(e).lat = peak(e).lat(hloc);
                                peak(e).amp = peak(e).amp(hloc);                        
                            end
                        end

                        if ~isnan(visual)
                            % if any(comp{1}=='N'); seg = -1*seg; end % joonistamiseks pilt ka tagasi p��rata
                            
                            % make a wider window for plotting with context
                            dataCall_w = dataCall;                                      % make a new datacall for wider window
                            dataCall_w = strrep(dataCall_w, 'ltind', 'ltind_w');
                            dataCall_w = strrep(dataCall_w, 'utind', 'utind_w');
                            
                            [a, ltind_w] = min(abs(inp.times-comp{c,3}(1)+100));        % find the indices of selected time windows mius 100 ms
                            [a, utind_w] = min(abs(inp.times-comp{c,3}(2)-300));
                            inptimes_w = inp.times(ltind_w:utind_w);                    % make a time vector
                            seg_w = squeeze(mean(eval(dataCall_w),1));                  % isolate the component for each subject 

                            for e = 1:size(seg,2)
                                figure; plot(inptimes_w,seg_w(:,e)); hold on; plot([peak(e).lat],[peak(e).amp],'o');
                                legend(strrep([inp.subs{e} ' ' fieldName], '_', ' '));
                                title('Kui pakkumine sobib, vajuta OK. Kui mitte, vii kursor �igesse kohta ja vajuta siis OK.');
                                datacursormode on; % legend(inp.subs);
                                
                                uicontrol('Style', 'pushbutton', 'String', 'OK', 'Position', [20 60 60 40], 'Callback', 'uiresume(gcf)');                                
                                uiwait(gcf);                                
                                g = getCursorInfo(datacursormode(gcf));
                                if ~isempty(g)
                                    g = g.Position
                                    peak(e).lat = g(1);
                                    peak(e).amp = g(2);
                                end
                                close(gcf);
                            end
                            
                        end

%                         if ~isnan(visual)
%                             figure; plot(inptimes,seg); hold on; plot([peak.lat],[peak.amp],'o'); title([fieldName 'PEAK']); % legend(inp.subs);
%                             uiwait; close(gcf);
%                         end

                        outf.([locType{:} '_pl'])(:,end+1) = cat(1,fieldName,mat2cell([peak.lat]',ones(size(data{1},3),1),1));                
                        outf.([locType{:} '_peakAmp'])(:,end+1) = cat(1,fieldName,mat2cell([peak.amp]',ones(size(data{1},3),1),1));                

                    end

                    if any(strcmp(latency,{'al', 'both'}))

                        seg = squeeze(mean(eval(dataCall),1));                  % isolate the component for each subject 
                        if any(comp{1}=='N'); seg = -1*seg; end                 % for negative components flip the waveform around

                        % fractional area latency % 
                        peak = struct();

                        % seg = seg - repmat(mean(seg(1:round(0.1*size(seg,1)),:)),[size(seg,1) 1]);  % first 10% of window length is used to substract as a local baseline
                        seg = seg - repmat(min(seg),[size(seg,1) 1]);                               % use the lowest point in the corrected windows as a local baseline   
                        % seg = seg - repmat(mean(seg([1 end],:)),[size(seg,1) 1]);                   % the average of first and last point of window length is used to substract as a local baseline

                        cumauc = cumtrapz(seg);                                                     % find the cumulative area under the curve for each participant (column)
                        auc = (cumauc./repmat(cumauc(end,:),[size(cumauc,1) 1]))>0.5;               % find the time points where auc is above 50% of it's final value in each column
                        for e = 1:size(auc,2)                                                       % find the first such timepoint for each subject
                            if ~isempty(find(auc(:,e)))
                                peak(e).lat = inptimes(find(auc(:,e),1));
                                peak(e).val = seg(find(auc(:,e),1),e);
                            end
                        end
                        outf.([locType{:} '_al'])(:,end+1) = cat(1,fieldName,mat2cell([peak.lat]',ones(size(data{1},3),1),1));

                        if ~isnan(visual)
                            figure; plot(inptimes,seg); hold on; plot([peak.lat],[peak.val],'o'); title([fieldName 'AREA']); % legend(inp.subs);
                            uiwait; close(gcf);
                        end
                    end
                end
                
            end                     
        end

        for f = fields(outf)'
            if isfield(inp, 'subs') && ~any(strfind(f{:},'singletrial'))
                outf.(f{:}) = cat(2,[{'subject'} inp.subs]',outf.(f{:}));
            end
            xlswrite(savefile,outf.(f{:}),[comp{c,1} f{:}])
%             if isempty(allout)
%                 allout = outf.(f{:});
%             else
%                 allout = cat(2,allout,outf.(f{:})(:,2:end));
%             end
        end
        
    end
    % xlswrite(savefile,allout,'allout'); 
        
end