This is a pipeline for analysis of EEG data

GENERAL INFORMATION


The analysis creates a few folders:

1. raw - these are the raw files.
2. import - these are raw files (with events) in EEGlab format
3. output:
 a. ICA - for each participant we have two files: .set file which has the metadata and the fdt files which has the actial EEG data
 the ica is the same raw files with the ICA solution (just a matrix) connceted to them . There is one potential difference which is if we drops files because they were too noisy,these parts would be missing.

 b. Epoch: this is the final folders. There are epoched (cut out segments)
   **excel sheet** - has a list of missing channel and counts of events remaining after the cut.
   we

   Andero is using these numbers for two things: a. to check if some participants are under utilized in the analysis (becaue we are using less than 50% of their data) and then to remove them.
   b. conducting an anova to comparing different significants in conditions.

   **data** holding the data in a matrix.


EPOCH AND CLEANING:

provides a few outpot for each participant:
1. The component map (different brains in colors): which basically provides the location of the signal that is responsibile for this factor.
2. Componen scroll plot a temporal representation of what we see in the component map. If we think about this in terms of factor analysis - these are the different factors. Only unlike factor analysis we have always a similar amount of factors and channels.
**this should be usually be reduced in resolution to ~30**
3. This is the channel scroll plot- which is just the raw data.

The aim in this stage is to find the components which are respnsible for one of three things:
1. eye blinks: big fast wave that happens in a similar amount of time. We would look for it in the component scroll lot. In in map we should see it as activation in the front.  
2. Horizontal sacads: platos in the scroll plot - in the map it should be activation at the front as well. For the Horizontal the platos are stronget but in the map it looks like a very sharp distinction between plus and minus in the frontal region.
3. Vertical sacads: sometimes doens't have a different component. If it does we should look for the platos as well (also less strong) and it looks like a sharp activation which is similar.

deep vs. shallow activation: can be detected by the strength of the activation in different brain regions. When the activation spreads that usually means that it influenced a lot of parts in the brain. However, when things are very local its easier to think about it as something that is happenning at the periphery.
people are calling that **dipolar** if the distribution are very symetrical than its better/
To sum, we have two critiria: a. how wide the activation is. b. how symetrical it is (dipolar). If we have these conditions its easier for us to detect brain based activation.

***important*** if we see really weird activation we should not reject them just to make sure that we are only rejecting stuff based on eye movement. We will do the cleaning later.

Build analysis structure:
creates a component p that compiles all the data. Up to that point the data is kept in seperate for each subject
