function epoch = epochList(EEG)

    epoch = struct();                                                       % genereerib trialitestruktuuri, kus oleks vaid �ks v��rtus triali kohta
    for e = 1:EEG.trials
        fld = fields(EEG.epoch)';
        for f = 2:length(fld)
            if iscell(EEG.epoch(e).(fld{f}))                                % kui on �hes trialis mitu eventi
                EEG.epoch(e).(fld{f}) = EEG.epoch(e).(fld{f}){1};
            end
            if ischar(EEG.epoch(e).(fld{f}))
                epoch(e).(fld{f}(6:end)) = strtrim(EEG.epoch(e).(fld{f}));  % kui on string, eemalda t�hikud
            else
                % epoch(e).(fld{f}(6:end)) = EEG.epoch(e).(fld{f});
                epoch(e).(fld{f}(6:end)) = num2str(EEG.epoch(e).(fld{f}));
            end
        end
    end
    
end