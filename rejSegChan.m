function EEG = rejSegChan(EEG, exchans, threshlimit, specrange, speclimit, chanlimit)
    % EEG = rejSegChan(EEG, exchans, threshlimit, specrange, speclimit)
    %   EEG - data structure
    %   exchans - cell with channel lables to be excluded from analysis and rejection 
    %   threshlimit - threshold rejection limit, applied in both direction (e.g. 100 = from -100 to 100)
    %   specrange - spectral rejection range (e.g. [15 30])
    %   speclimit - lowe and upper limits for spectral rejection (e.g. [-100 25])
    %   chanlimit - proportion of trial rejections caused by a single
    %               channel (default 0.02)
    %   if spectral rejection parameters are omitted, performs only the
    %   thershold rejection
    % 
    %   Andero Uusberg (andero.uusberg@ut.ee)

    if nargin < 6
        chanlimit = 0.02;
    end
    
    reject = EEG.reject;                                                                    % make a copy for unrejected base structure for restoring later
    [a, datachans] = setdiff({EEG.chanlocs.labels}, exchans);                               % find the indices of the channels to be analyzed
    datachans = datachans';
    
    EEG.rejhist.epochChan = []; manrej = [];                                                % initiate some counters
    
    EEG = pop_eegthresh(EEG, 1, datachans,-threshlimit,threshlimit,EEG.xmin,EEG.xmax,0,0);  % find segments violatung the threshold rejection criterion
    
    if nargin > 3 && ~isempty(specrange)                                                    % if spectral rejection is also specified in the arguments
        EEG = pop_rejspec(EEG, 1,'elecrange',datachans,'threshold',[speclimit(1) speclimit(2)],'freqlimits',specrange); %  ,...
            % 'eegplotcom','set(findobj(''parent'', findobj(''tag'', ''rejtrialraw''), ''tag'', ''mantrial''), ''string'', num2str(sum(EEG.reject.rejmanual)));set(findobj(''parent'', findobj(''tag'', ''rejtrialraw''), ''tag'', ''threshtrial''), ''string'', num2str(sum(EEG.reject.rejthresh)));set(findobj(''parent'', findobj(''tag'', ''rejtrialraw''), ''tag'', ''freqtrial''), ''string'', num2str(sum(EEG.reject.rejfreq)));set(findobj(''parent'', findobj(''tag'', ''rejtrialraw''), ''tag'', ''consttrial''), ''string'', num2str(sum(EEG.reject.rejconst)));set(findobj(''parent'', findobj(''tag'', ''rejtrialraw''), ''tag'', ''enttrial''), ''string'', num2str(sum(EEG.reject.rejjp)));set(findobj(''parent'', findobj(''tag'', ''rejtrialraw''), ''tag'', ''kurttrial''), ''string'', num2str(sum(EEG.reject.rejkurt)));','eegplotplotallrej',2,'eegplotreject',0); close(gcf); close(gcf);
                                                                                            % currently only this lengthy call to the arguments seems to mark, but not reject the segments violating the spectral rejection criterion        
        rejmat = EEG.reject.rejthreshE | EEG.reject.rejfreqE;                               % make a unified channel-by-epoch matrix of rejection decisions
    else                                                                                    % if only threshold rejection is required
        rejmat = EEG.reject.rejthreshE;                                                     % use the matric from threshold method
    end
        
    for i = 1:size(rejmat,1);                                                               % mark for removal the channels containing no retained segments at all
        if length(find(rejmat(i,:)))==size(rejmat,2); 
            manrej = [manrej i]; 
        end 
    end
    rejmat(manrej,:) = 0;                                                                   % remove those channels from the following screening

    rejCause = [];                                                                          % find channels that are exclusively responsible for criterion violations in some segments
    for i = 1:size(rejmat,2); 
        if length(find(rejmat(:,i)))==1; 
            rejCause = [rejCause find(rejmat(:,i))]; 
        end 
    end

    if any(rejCause)
        for j = unique(rejCause);                                                               % mark for removal the channels that exlusively cause the rejection of more than 2% of remaining trials
            if length(find(rejCause == j))/EEG.trials > chanlimit; 
                manrej = [manrej j]; 
            end; 
        end 
    end
    
    if ~isempty(manrej)
        EEG.reject = reject;                                                                % empty the previous marks just in case
        EEG = pop_select(EEG,'nochannel',manrej);                                           % remove the marked channels
        EEG.rejhist.epochChan = manrej;                                                     % retain their numbers in a special field
        [a, datachans] = setdiff({EEG.chanlocs.labels}, exchans);                           % find new indexes for the channels to be analyzed
        datachans = datachans';
        
        EEG = pop_eegthresh(EEG,1,datachans,-threshlimit,threshlimit,EEG.xmin,EEG.xmax,0,0);% run the threshold rejection again
        if nargin > 3 && ~isempty(specrange) 
            EEG = pop_rejspec(EEG, 1,'elecrange',datachans,'threshold',[speclimit(1) speclimit(2)],'freqlimits',specrange ,'eegplotcom','set(findobj(''parent'', findobj(''tag'', ''rejtrialraw''), ''tag'', ''mantrial''), ''string'', num2str(sum(EEG.reject.rejmanual)));set(findobj(''parent'', findobj(''tag'', ''rejtrialraw''), ''tag'', ''threshtrial''), ''string'', num2str(sum(EEG.reject.rejthresh)));set(findobj(''parent'', findobj(''tag'', ''rejtrialraw''), ''tag'', ''freqtrial''), ''string'', num2str(sum(EEG.reject.rejfreq)));set(findobj(''parent'', findobj(''tag'', ''rejtrialraw''), ''tag'', ''consttrial''), ''string'', num2str(sum(EEG.reject.rejconst)));set(findobj(''parent'', findobj(''tag'', ''rejtrialraw''), ''tag'', ''enttrial''), ''string'', num2str(sum(EEG.reject.rejjp)));set(findobj(''parent'', findobj(''tag'', ''rejtrialraw''), ''tag'', ''kurttrial''), ''string'', num2str(sum(EEG.reject.rejkurt)));','eegplotplotallrej',2,'eegplotreject',0); close(gcf); close(gcf);                                                                           % if specified, run the spectral rejection again
        end
    end
    
    EEG.rejhist.thresh = find(EEG.reject.rejthresh);                                        % write the numbers of rejected segments
    if nargin > 3 && ~isempty(specrange)
        EEG.rejhist.spec = find(EEG.reject.rejfreq);                                        % write the numbers of rejected segmenst
        EEG = pop_rejepoch(EEG, union(EEG.rejhist.thresh,EEG.rejhist.spec),0);              % reject the segments
    else
        EEG = pop_rejepoch(EEG, EEG.rejhist.thresh,0);                                      % reject the segments
    end
    
end