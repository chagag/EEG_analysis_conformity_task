function plotERPeffect(plot, dim2plot, design, pval, correct, timewindow, baseline, type, save);

%%    ARGUMENTS
%     plotERPeffect(data, dim2plot, design, pval, correct, timewindow, baseline, type, save)
%
%     data - structure with following fields
%         data - cell of CHANNEL x TIME x SUBJECT ERP matrixes corresponding to factorial design 
%         labels - substructure holding factor name in fieldname and level labels in a cell
%         chanlocs - EEGLAB channel location structure corresponding to number of channels;
%         times - EEGLAB times vector corresponding to TIME
%         subs - subjects
%
%     dim2plot - the dimension to be analysed, if vector compares the
%     difference between the first and last levels of the second dimension
%     within levels of the 1st dimension
%     design - a cell specifying the factor levels to be included; EX: {1:2,1:2,1,2,1:2} or {':', ':', 1, 2, ':'}
%     pval - p-value; default: 0.05 
%     correction - 'fdr' - False Discovery Rate correction, anything else
%     (e.g. 'uncorrected') leaves unchanged; default fdr; 'cluster'
%     timewindow - vector with beginning and end of plotted time window
%     baseline - optional new baseline substraction
%     type
%           erp - compares all levels of dim2plot    
%           scalpDiff - contrastes last - first level of dim2plot on average timewindow
%           scalpComp - average in relation to zero
%           envp - plots grand avrerage data in all channels 
%     save - optional save to disk
    
    if nargin < 4                                                           % default p-level
        pval = 0.05;
    end
    if nargin < 5                                                           % default correction
        correct = 'fdr';
    end
    if nargin < 6                                                           % default timewindow (whole data duration)
        timewindow = [plot.times(1) plot.times(end)];
    end
    if nargin < 7                                                           % default baseline
        baseline = [-200 0];
    end
    if nargin < 8                                                           % default figure type
        type = 'erp';
    end
    if length(dim2plot) > 1                                                 % if requires an interaction
        dim2subtract = dim2plot(2);                                         % define the dimension to subtract
        dim2plot = dim2plot(1);                                             % define the dimension to compare
    else
        dim2subtract = NaN;                                                 % if only main effect
    end
    
%%  SELECT DATA %%

    if timewindow(1) < plot.times(1)                                        % korrigeerib valikuakna olemasolevate andmepunktide pikkuseks
        timewindow(1) = plot.times(1);
    end
    if timewindow(2) > plot.times(end)
        timewindow(2) = plot.times(end);
    end
    
    [a, ltind] = min(abs(plot.times-timewindow(1)));                        % find the indices of selected time windows
    [a, utind] = min(abs(plot.times-timewindow(2)));
    plottimes = plot.times(ltind:utind);                                    % make a shortened time vector

    if ~isempty(baseline)
        [a, lbsl] = min(abs(plot.times-baseline(1)));                       % find the indices of selected baseline
        [a, ubsl] = min(abs(plot.times-baseline(2)));
    end
    data = plot.data(design{:});    

    dims = length(size(data));                                              % find the total number of data dimensions
    S.type = '()';                                                          % define a required parameter for subsref
    for n = 1:size(data,dim2plot);                                          % average the data for the levels to be compared
        S.subs = {};                                                        % initiate the reference structure
        for d = 1:dims;                                                     % loop through dimensions
            if d == dim2plot                                                % if selected dimension is to be plotted...
                S.subs = [S.subs {n}];                                      % ... include the level in the selection structure                
            elseif d == dim2subtract                                        % else if the dimension needs to be subtracted...
                S.subs = [S.subs 1];                                        % ... mark the first level to be substratced from (also this is the only level to be averaged later)
            else                                                            % else if the dimension is to be averaged...
                S.subs = [S.subs ':'];                                      % mark both levels for averageing
            end
        end
        
        if ~isnan(dim2subtract)
            S.type = '{}'; S2 = S; S2.subs{dim2subtract} = size(data,dim2subtract);        % make a reference structure for isolating the last levels of dim2subtract
            a = {mean(cat(4,subsref(data, S)),4)-mean(cat(4,subsref(data, S2)),4)};              % perform the subtraction 
        else
            a = subsref(data,S);                                            % select only the cells specified by design argument
        end        
        a = mean(cat(4,a{:}),4);                                            % select a level of required comparison and average remaining factors
        if ~isempty(baseline)
            a = a - repmat(mean(a(:,lbsl:ubsl,:),2),[1,size(a,2),1]);       % remove baseline
        end
        dimdata{n} = a(:,ltind:utind,:);                                    % generate the cell with two levels of selected time window data
    end

%% STATISTICS %%


    loc.roi = []; loc.ch = [];
    for c = 1:length(plot.chanlocs)
        if length(plot.chanlocs(c).labels) > 3                              % leiab ROId nende sildi pikkuse alusel
            % loc.roi = [loc.roi c];
        else
            loc.ch = [loc.ch c];
        end
    end

    if strcmp(correct,'cluster')
        
        % MAKE FIELDTRIP ELECTRODE LAYOUT AND NEIGHBOURS STRUCTURE
        a = struct();
        a.elec.pnt = [plot.chanlocs.X; plot.chanlocs.Y; plot.chanlocs.Z]';
        [a.elec.label, a.label] = deal({plot.chanlocs.labels});
        layout = ft_prepare_layout(a);
        neighbours = ft_prepare_neighbours(struct('method', {'distance'}, 'neighbourdist', {8}, 'layout', {layout}, 'channel', {a.label}, 'elec', {a.elec}, 'feedback', {'no'}));

        labels = fields(plot.labels);

        cfg.design = []; cfg.command = 'ft_timelockstatistics(cfg,';
        d = struct();
        for cond = 1:length(dimdata)
            name = strrep(plot.labels.(labels{dim2plot}){cond},' ','_');
            d.(name).avg = permute(dimdata{cond},[3 1 2]);
            d.(name).dimord = 'subj_chan_time';
            d.(name).label = a.elec.label;
            d.(name).time = plottimes;
            for sub = 1:size(dimdata{1},3); 
                cfg.design(1,end+1) = sub; 
                cfg.design(2,end) = cond; 
            end
            cfg.command = [cfg.command, 'd.', name, ','];
        end
        cfg.command = [cfg.command(1:end-1),');'];
        
        % COMPUTE PREMUTATION-BASED STATISTICS
        cfg.channel          = a.elec.label;                                        % subset to be analyzed (could also be 'all')
        cfg.latency          = [plottimes(1) plottimes(end)];

        cfg.correctm         = 'cluster';                                           % select the clustering approach
        cfg.method           = 'montecarlo';
        cfg.numrandomization = 500;

        if length(dimdata)>2
            cfg.statistic        = 'ft_statfun_depsamplesFmultivariate';            % lower level statistics (used for forming clusters, not corrected for MCP)
            cfg.tail             = 1;
            cfg.clustertail      = 1;
        else
            cfg.statistic        = 'ft_statfun_depsamplesT';                        % lower level statistics (used for forming clusters, not corrected for MCP)
            cfg.correcttail      = 'prob';                                          % see http://www.fieldtriptoolbox.org/faq/why_should_i_use_the_cfg.correcttail_option_when_using_statistics_montecarlo
        end
        cfg.alpha            = 0.05;
        cfg.clusterstatistic = 'maxsum';                                            % testing the significance of clusters
        cfg.clusteralpha     = 0.05;
        cfg.neighbours       = neighbours;

        if ~isempty(loc.roi);                                                       % if selected, perform False Discovery Rate correction (separately on ROIs)
            cfg.channel          = loc.roi;                                         % subset to be analyzed (could also be 'all')        
            cfg.neighbours       = [];
        end
        
        cfg.uvar             = 1;
        cfg.ivar             = 2;
        
        stats = eval(cfg.command);
        pvals = stats.prob;
        
        if ~isempty(loc.roi);                                                       % make the pvals structure larger so that it would not crash subsequent functions
            a = zeros(size(mean(dimdata{1},3)));
            a(loc.roi,:) = pvals;
            pvals = a;
        end

    else
        
        [stats, df, pvals] = statcond(dimdata, 'method', 'param');                  % calculate statistics for the analysed effect
        
    end

    
    if strcmp(correct,'fdr');                                                       % if selected, perform False Discovery Rate correction (separately on ROIs)
        for r = 1:length(loc.roi)
            pvals(loc.roi,r) = fdr(pvals(loc.roi,r));
        end
        pvals(loc.ch,:) = fdr(pvals(loc.ch,:));
    end
    
    pmask = {};                                                             % leiab statistiliselt olulised kohad
    for i = 1:size(pvals,1)                                                 % loop through channels
        maskrow = find(pvals(i,:) < pval);
        if isempty(maskrow)                                                 % no significant points
            pmask{i} = [];
        else
            pmask{i}(1,1) = plottimes(maskrow(1));                          % the first significant point
            for j = 2:length(maskrow)                                       % loop through the rest
                if maskrow(j)-maskrow(j-1) > 1                              % if not consequitive...
                   pmask{i}(2,end) = plottimes(maskrow(j-1));               % ... end the opened significance section with the 1st one
                   pmask{i}(1,end+1) = plottimes(maskrow(j));               % ... and start a new one
                else                                                        % if consequitive...
                   pmask{i}(2,end) = plottimes(maskrow(j));                 % ...end it with the 2nd one
                end
            end
            pmask{i}(2,end) = plottimes(maskrow(end));                      % end the last opening one
        end
    end
    
%% PLOT %% 
    data2plot = [];                                                         % make a CHAN x TIME x CONDITION matrix for topoplot
    for i = 1:length(dimdata);                                              % 
        data2plot(:,:,i) = mean(dimdata{i},3);                              % average subjects

%         for ch = 1:size(data2plot,1)                                        % optional smoothind
%             data2plot(ch,:,i) = smooth(data2plot(ch,:,i),7);                % moving average over 5 points
%         end
        
    end

    col = {{'b--' 'linewidth' 2}...
           {'r' 'linewidth' 2}... 
           {'k' 'linewidth' 2}...
           {'r--' 'linewidth' 2}...
           {'k--' 'linewidth' 2}...
           {'b' 'linewidth' 2}...
           {'g' 'linewidth' 2}...
           {'g--' 'linewidth' 2}};                                            % set line color system

    labels = fields(plot.labels);                                           % make a cell of labels structure field names 
    title = labels{dim2plot};                                               % make the factor name the title
    if isfield(plot,'name')
        title = [plot.name, ' ', title];
    end
    legend = plot.labels.(labels{dim2plot})(design{dim2plot});              % select the level names as waveform labels

    interaction = '';                                                       % generate labels for limited designs
    for d = 1:length(design);                                               % cycle through factors
        if d == dim2plot                                                    % skip the factor being analysed
        else
            if ischar(design{d}) || size(plot.data,d) == length(design{d})  % if all levels are not included
            else
                interaction = ['@', labels{d},':', plot.labels.(labels{d}){design{d}} interaction];
            end
        end
    end

    heading = strcat(title,'',interaction,...                               % construct the heading
    ' n',num2str(size(dimdata{1},3)),...
    ' p',num2str(pval),...
    ' ', correct); 

    if strcmp(type, 'erp')
        limits = [timewindow(1) timewindow(2) 0 0];
        figure; plottopo(data2plot,'chanlocs', plot.chanlocs, 'ydir', 1, 'limits', limits, 'regions', pmask, 'title', heading, 'legend', legend, 'colors', col(1:size(data2plot,3))); % joonistamine
        set(gcf, 'name', heading); % 'Color', [1 1 1] ,        
    elseif strcmp(type, 'scalpDiff')
        scalpdata = squeeze(mean(data2plot,2));                             % keskmistab ajadimensiooni
        scalpdata = scalpdata(:,end)-scalpdata(:,1);                        % kontrast �le inimeste keskmistatud
        figure; topoplot(double(squeeze(mean(pvals,2))<pval).*scalpdata,plot.chanlocs,'electrodes','on', 'style', 'map', 'whitebk', {'on'},'shading', 'interp');
        set(gcf, 'Name', heading);
    elseif strcmp(type, 'scalpComp')
        scalpdata = squeeze(mean(mean(data2plot,2),3));                     % keskmistab ajadimensiooni
        % scalpdata = zscore(scalpdata);
        figure; topoplot(scalpdata,plot.chanlocs,'electrodes','on', 'style', 'map', 'whitebk', {'on'},'shading', 'interp');
        set(gcf, 'Name', heading);
    elseif strcmp(type, 'envp')
        data2plot = squeeze(mean(data2plot,3));                             % keskmistab �le tingimuse
        envp = {};
        for c = 1:size(data2plot,1)
            envp{c} = data2plot(c,:)';
        end
        std_plotcurve(plottimes, envp, 'plotgroups', 'together' , 'titles', {'envelope'}, 'legend', {plot.chanlocs.labels})
    end        
 
%     if isfield(vars,'outdir')
%         saveas(gcf,[vars.outdir,[heading{:},'.fig']]);
%     end
    
end
