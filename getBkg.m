function bkg = getBkg(EEG, des, bkg, subRemove, chanlocs)
    
    if nargin < 5
        chanlocs = readlocs('L:\eeglabtools\32_4EOG.ced', 'filetype', 'autodetect');                    % elektroodide asukohad     
    end
    
    if nargin<4
        subRemove = 0;
    end
    
    doHead = 0;
    if isempty(bkg)
        bkg = {'subject' chanlocs.labels};
        doHead = 1;
    end
    
    bkg(end+1,1:length(chanlocs)+1) = [{EEG.subject} mat2cell(ones(1,length(chanlocs)), 1, ones(1,length(chanlocs)))];    % kirjutab k�ik kanalid olemasolevaks
    [a, a] = setdiff({chanlocs.labels}, {EEG.chanlocs.labels});
    bkg(end,a+1) = {NaN};                                                   % kustutab puuduvad
        
    epoch = EEG.event(1); for e = 1:length(EEG.epoch); epoch(e) = EEG.event(EEG.epoch(e).event(1)); end

    lbls = {}; max = [];
    vars = {}; inds = {};                                                   % make a list of concatenated labels and indices using the "odometer" method

    factors = fields(des)';
    for f = 1:length(factors)                                               % loop throough factors
        levels = des.(factors{f});                                          
        for l = 1:length(levels);                                           % loop through levels of the given factor
            lbls{f,l} = levels{l};                                          % populate a cell of level labels
        end
        max(f) = l;
        
        for e = 1:length(epoch)                                             % if levels happen to be numeric, convert to text
            epoch(e).(factors{f}) = num2str(epoch(e).(factors{f}));
        end

    end
    
    
    at = ones(1,size(lbls,1));                                              % min and max levels (the former will cycle)
    while length(vars) < prod(max);                                         % repeat until all possible levels have been found
        row.L = ''; row.D = 1;                                              % initiate row counters
        for i = 1:length(at)                                                % cycle through all avaailable variables
            row.L = [row.L '_' lbls{i,at(i)}];                              % generate a label for the given variable and level
            row.D = row.D.*strcmp(lbls{i,at(i)},{epoch.(factors{i})});      % generate indices for the given variable and level
        end
        vars{end+1} = row.L(2:end);                                         % add the generated rows to the growing lists
        inds{end+1} = sum(row.D);
        
        % add psychometrics here
        % split-half reliability
        
        at(end) = at(end) + 1;                                              % shift the last ring on the odometer
        for i = length(at):-1:1                                             % cycle through all variables
            if at(i) > max(i);                                              % if any of them has reached their limit, reset this one and shift the previous one to next level
                at(i) = 1;
                if i > 1
                    at(i-1) = at(i-1)+1;          
                end
            end
        end
    end
    
    if doHead == 1; 
        bkg(1,end+1:end+1+length(vars)) = [vars {'include'}]; 
    end
    
    bkg(end,length(chanlocs)+2:end) = [inds {1}];
    if min([inds{:}]) < subRemove;
        bkg{end,end} = 0;                                                                                     % mark as rejected in bkg last column
    end
end