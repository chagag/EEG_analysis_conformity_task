function EEG = selBadICA(EEG, event, timeRange)

    ALLEEG = EEG; CURRENTSET = 1;

    for e = 1:length(EEG.event)
        if isnumeric(EEG.event(e).type)                                                 % mingis andmestikus oli vaja event-v�ljade t��pide �htlustamist
            EEG.event(e).type = num2str(EEG.event(e).type)
        end
    end
    [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET); 
    
    [ALLEEG EEG CURRENTSET] = pop_copyset(ALLEEG, 1, 2);                                % komponente on parem vaadata segmenteeritud andmetel, teeb selleks dataseti koopia
    [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);                           

    EEG = pop_epoch(EEG, {event}, timeRange, 'epochinfo', 'yes');                       % teeb sobivad segmendid
    EEG = pop_rmbase(EEG, [-200 0]);                                                    
    [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);                           
        
    pop_eegplot(EEG, 0, 1, 1); pop_eegplot(EEG, 1, 1, 1);                               % avab vajalikud plotid ja paneb need p�rast OK vajutamist kinni
    if length(EEG.reject.gcompreject)>35
        pop_selectcomps(EEG, [1:35])
    else
        pop_selectcomps(EEG, [1:length(EEG.reject.gcompreject)])
    end
    uiwait; close(gcf); close(gcf); 
    
    ch = inputdlg('pilgutuse komponent'); 
    ALLEEG(1).rejhist.blinkComp = str2num(ch{:});
    
    ch = inputdlg('horisontaalne liigutus'); 
    ALLEEG(1).rejhist.horMoveComp = str2num(ch{:});
    
    ALLEEG(1).reject.gcompreject = EEG.reject.gcompreject;                              % kirjutab identifitseeritud komponendid segmenteerimata andmestikku           
            
    % compute IAF if neccessary 
    if ~isfield(EEG,'IAF') || isempty(EEG.IAF)                                          % k�sitsi m��ramine: otsi joonisel �les suurim v�i lihtsalt alfam tipp vahemikus 17 (8 Hz) - 27 (13 Hz), pane joonis kinni ja kirjuta tipu x-koordnat j�rgmisena ilmuvasse kasti)
        EEG = pop_rmbase(EEG, []);
        [a, postChans] = intersect({EEG.chanlocs.labels},{'O1', 'Oz', 'O2', 'P3', 'P4', 'P7', 'P8', 'CP1', 'CP2', 'CP5', 'CP6'}); % find IAF from posterior electrodes; 
        [spect, sfrqs] = spectopo(EEG.data(postChans,:,:), EEG.pnts, EEG.srate, 'winsize', EEG.srate, 'overlap', EEG.srate/2, 'plot', 'off', 'precent', 100); % 0.5 Hz resolutsiooniga spektrogramm �le tagumiste kanalite
        figure; plot(mean(spect(:,1:find(round(sfrqs*2)/2==20)))); uiwait; IAF = inputdlg; 
        ALLEEG(1).IAF = round(sfrqs(str2num(IAF{:}))*2)/2;
    end
    
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, CURRENTSET, 'retrieve', 1);       % aktiveerib segmenteerimata andmestiku
    EEG = pop_saveset(EEG, 'filename', EEG.filename, 'filepath', EEG.filepath, 'savemode', 'twofiles'); % salvestab ICA kausta tagasi
    f.name = EEG.filename; f.path = EEG.filepath;
    ALLEEG = []; EEG = []; 
    EEG = pop_loadset('filename', f.name, 'filepath', f.path);                 % t�hjendab igaks juhuks ja avab uusti
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, CURRENTSET, 'setname', EEG.subject); 
end